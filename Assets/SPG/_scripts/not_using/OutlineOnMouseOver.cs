﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineOnMouseOver : MonoBehaviour {

    public delegate void EnemyHighlight(Transform target);
    public static event EnemyHighlight OnEnemyHighlight;
       
    public void OnMouseOver()
    {
        SkinnedMeshRenderer[] renderers = GetComponentsInChildren<SkinnedMeshRenderer>();
        foreach (SkinnedMeshRenderer renderer in renderers)
        {
            if (renderer.material.shader.name == "Piratas/Standard Outline")
            {
                renderer.material.SetFloat("_Outline", 0.5f);
            }
            //if (renderer.material.shader.name == "cofre/standard outline")
        }

        if (OnEnemyHighlight != null)
        {
            OnEnemyHighlight(GetComponent<Transform>());
        }
    }

    public void OnMouseExit()
    {
        SkinnedMeshRenderer[] renderers = GetComponentsInChildren<SkinnedMeshRenderer>();
        foreach (SkinnedMeshRenderer renderer in renderers)
        {
            if (renderer.material.shader.name == "Piratas/Standard Outline")
            {
                renderer.material.SetFloat("_Outline", 0.0f);
            }
        }
        if (OnEnemyHighlight != null)
        {
            OnEnemyHighlight(null);
        }
    }
}
