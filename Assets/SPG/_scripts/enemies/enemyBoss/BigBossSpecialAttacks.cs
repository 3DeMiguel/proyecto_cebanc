﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigBossSpecialAttacks : MonoBehaviour {

    private EnemyBossSight _enemyBossSight;
    private Animator _animator;
    private UnityEngine.AI.NavMeshAgent _navMeshAgent;

    // Use this for initialization
    void Start () {
        _enemyBossSight = GetComponentInChildren<EnemyBossSight>();
        _animator = GetComponent<Animator>();
        _navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Vector3.Distance(_enemyBossSight._player.transform.position, transform.position) >= 30 && Vector3.Distance(_enemyBossSight._player.transform.position, transform.position) <= 40)
        {
            _animator.SetTrigger("Charging");
            _animator.SetBool("Walking", false);

        }
        else
        {
            _animator.SetBool("Charging", false);
        }
    }

    public void Charge()
    {
        _navMeshAgent.SetDestination(_enemyBossSight._player.transform.position);
        _navMeshAgent.speed *= 5;
    }
}
