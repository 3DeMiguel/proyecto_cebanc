﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBossSight : MonoBehaviour {
    public bool playerInSight = false;

    public GameObject _player;

    public int _enemyDetectionDistance = 40;

    // Use this for initialization
    void Awake () {
        _player = GameObject.FindGameObjectWithTag("Player");
	}
}
