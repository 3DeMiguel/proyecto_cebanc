﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBossWalk : MonoBehaviour {

    public float _enemySpeed;

    public Animator _animator;

    public UnityEngine.AI.NavMeshAgent navMeshAgent;
    private EnemyBossSight _enemyBossSight;
    private EnemyBossAttack _enemyBossAttack;
    private BigBossSpecialAttacks _BigBossSpecialAttacks;
    private EnemyBossHP _enemyBossHP;
    public Transform _playerTransform;

    // Use this for initialization
    void Start () {
        navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        _enemyBossSight = GetComponentInChildren<EnemyBossSight>();
        _enemyBossHP = GetComponent<EnemyBossHP>();
        _enemyBossAttack = GetComponent<EnemyBossAttack>();
        _BigBossSpecialAttacks = GetComponent<BigBossSpecialAttacks>();
        navMeshAgent.speed = _enemySpeed;
	}
	
	// Update is called once per frame
	void Update () {

        if (Vector3.Distance(_enemyBossSight._player.transform.position, transform.position) <= _enemyBossSight._enemyDetectionDistance)
        {
            _enemyBossSight.playerInSight = true;
        }
        else
        {
           _enemyBossSight.playerInSight = false;
        }

        if (_enemyBossSight.playerInSight == false && _enemyBossHP._dead==false)
        {
            navMeshAgent.Stop();
            _animator.SetBool("Walking", false);
        }
        else if (_enemyBossSight.playerInSight == true && _enemyBossAttack._inMeleeRange == false && _enemyBossHP._dead == false)
        {
            navMeshAgent.Resume();
            navMeshAgent.SetDestination(_enemyBossSight._player.transform.position);
            _animator.SetBool("Walking", true);
        }
        else if (_enemyBossSight.playerInSight == true && _enemyBossAttack._inMeleeRange == true && _enemyBossHP._dead == false)
        {
            navMeshAgent.Stop();
            transform.LookAt(_playerTransform);
            _animator.SetBool("Walking", false);
        }
	}
}
