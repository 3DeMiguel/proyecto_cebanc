﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBossShotAt : MonoBehaviour {

    public Animator _animator;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "bullet")
        {
            _animator.SetTrigger("Death");
        }
    }
}
