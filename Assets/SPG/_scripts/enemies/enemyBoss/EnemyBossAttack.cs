﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBossAttack : MonoBehaviour {

    public float _meleeRange =5f;
    public bool _inMeleeRange = false;

    private EnemyBossSight _enemyBossSight;
    private Animator _animator;
    //private UnityEngine.AI.NavMeshAgent _navMeshAgent;

    public GameObject attack1Box;

    // Use this for initialization
    void Start () {
        _enemyBossSight = GetComponentInChildren<EnemyBossSight>();
        _animator = GetComponent<Animator>();
        //_navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }
	
	// Update is called once per frame
	void Update () {
		if (Vector3.Distance(_enemyBossSight._player.transform.position, transform.position) <= _meleeRange)
        {
            _inMeleeRange = true;

            if (_inMeleeRange)
            {
                EnemyMelee();
            }
        }
        else
        {
            _inMeleeRange = false;
             if (_inMeleeRange==false)
            {
                EnemyOutOfMelee();
            }
        }
	}

    private void EnemyMelee()
    {
        _animator.SetBool("Attacking", true);
    }
    private void EnemyOutOfMelee()
    {
        _animator.SetBool("Attacking", false);
    }

    public void AttackBoxOn()
    {
        attack1Box.gameObject.SetActive(true);
    }

    public void AttackBoxOff()
    {
        attack1Box.gameObject.SetActive(false);
    }
}
