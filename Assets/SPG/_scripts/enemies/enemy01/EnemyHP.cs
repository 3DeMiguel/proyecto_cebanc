﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class EnemyHP : MonoBehaviour
{

    private float _enemyHP = 100f;
    public float _currentHP;

    private GameController _gameController;

    public Slider _slider;
    public Image _fillImage;
    public BoxCollider _boxCollider;

    private NavMeshAgent _navMeshAgent;

    private Canvas _canvasSlider;

    public Color _fullHPColor = Color.green;
    public Color _zeroHPColor = Color.red;

    public Animator _animator;

    public Camera _camera;

    public bool _dead;

    //variables para lanzar item de vida cuando muere
    public GameObject _prefabItemLive;
    public Vector3 _direccionSalida;
    public float _velocidadSalida;
    public Transform _transformInstanciaItem;

    //variables para las particulas de daño y muerte
    public ParticleSystem _particlesHit;
    public ParticleSystem _particlesDead;
    //audios para las particulas
    public AudioSource _particlesDeadAudio;
    public AudioSource _particlesHitAudio;

    // Use this for initialization
    void Start()
    {
        _currentHP = _enemyHP;
        _canvasSlider = GetComponentInChildren<Canvas>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _gameController = FindObjectOfType<GameController>();
        _camera = FindObjectOfType<Camera>();
    }

    void Update()
    {
        _canvasSlider.transform.rotation = _camera.transform.rotation;
        //_canvasSlider.transform.eulerAngles = new Vector3(_camera.transform.eulerAngles.x, _camera.transform.eulerAngles.y, _camera.transform.eulerAngles.z);
    }

    private void OnCollisionEnter(Collision collision)
    {
        //if (collision.gameObject.tag == "PlayerAttackBox")
        //{
        //añado las particulas de golpe
        //AddParticlesHit();
        //_animator.SetTrigger("GetHit");
        //_currentHP = _currentHP -90f;
        //SetHealthUI();

        //if (_currentHP <= 0f && !_dead)
        //{
        //  Kill();
        //}
        //}
        //else
        if (collision.gameObject.tag == "bullet")
        {
            //añado las particulas de golpe
            AddParticlesHit();
            _animator.SetTrigger("GetHit");
            _currentHP -= 80f;
            SetHealthUI();

            if (_currentHP <= 0 && !_dead)
            {
                Kill();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PlayerAttackBox")
        {
            //añado las particulas de golpe
            AddParticlesHit();
            //_animator.SetTrigger("GetHit");
            _currentHP -= 90f;
            SetHealthUI();

            if (_currentHP <= 0f && !_dead)
            {
                Kill();
            }
        }
        if (other.gameObject.tag == "PlayerAttackTwisterBox")
        {
            //añado las particulas de golpe
            AddParticlesHit();
            //_animator.SetTrigger("GetHit");
            _currentHP -= 100f;
            SetHealthUI();

            if (_currentHP <= 0f && !_dead)
            {
                Kill();
            }
        }
    }

    private void AddParticlesHit()
    {
        Debug.Log("emision de particulas hit");

        //hacemos visible las particulas
        //_particlesHit.gameObject.SetActive(true);
        //sonido de particulas
        if (_particlesHit != null)
        {
            _particlesHit.Play();
        }
        //corrutina para ver la animaconde las particulas entera 
        StartCoroutine(StopParticles(_particlesHit.main.duration));
        //_particlesHitAudio.Play();

    }
    private void AddParticlesDead()
    {
        Debug.Log("emision de particulas dead");
        _particlesDead.Play();
        //hacemos visible las particulas
        _particlesDead.gameObject.SetActive(true);
        //sonido de particulas de muerte
        // _particlesDeadAudio.Play();
    }

    IEnumerator StopParticles(float duration)
    {
        yield return new WaitForSeconds(duration);
        if (_particlesHit != null)
        {
            _particlesHit.Stop();
        }
        //_particlesHit.gameObject.SetActive(false);
    }

    private void SetHealthUI()
    {
        _slider.value = _currentHP;
    }

    private void Kill()
    {
        //hacemos visibles las particulas de muerte
        AddParticlesDead();
        _canvasSlider.enabled = false;
        _boxCollider.enabled = false;
        _navMeshAgent.enabled = false;
        _dead = true;
        _animator.SetTrigger("Death");
        Destroy(gameObject, 6f);
        _gameController.OnSumaExperience();
        ThrowLive();
    }

    private void ThrowLive()
    {
        //instanciamos el prefab del item    
        GameObject Item = (GameObject)Instantiate(_prefabItemLive, _transformInstanciaItem.position, _transformInstanciaItem.rotation);
        Item.transform.parent = transform;
        //recuperamos el rigidBody del item instanciado
        Rigidbody RigidBodyItem = Item.GetComponent<Rigidbody>();


        //aplicamos fuerza al item
        _direccionSalida.x = Random.Range(_direccionSalida.x - 0.2f, _direccionSalida.x + 0.2f);
        if (RigidBodyItem != null)
        {
            RigidBodyItem.AddForce(transform.TransformDirection(_direccionSalida * _velocidadSalida));
        }

        //SONIDO
        // _audioSource.PlayOneShot(_audios._LiveAudio);            
    }
}
