﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWalk : MonoBehaviour {

    public float _enemySpeed;

    public Animator _animator;

    public UnityEngine.AI.NavMeshAgent navMeshAgent;
    private EnemySight _enemySight;
    private EnemyAttack _enemyAttack;
    private EnemyHP _enemyHP;
    private Transform _transform;
    public Transform _playerTransform;

    // Use this for initialization
    void Start () {
        _transform = GetComponent<Transform>();
        navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        _enemySight = GetComponentInChildren<EnemySight>();
        _enemyHP = GetComponent<EnemyHP>();
        _enemyAttack = GetComponent<EnemyAttack>();
        navMeshAgent.speed = _enemySpeed;
	}
	
	// Update is called once per frame
	void Update () {

        if (Vector3.Distance(_enemySight._player.transform.position, transform.position) <= _enemySight._enemyDetectionDistance)
        {
            _enemySight.playerInSight = true;
        }
        else
        {
            _enemySight.playerInSight = false;
        }

        if (_enemySight.playerInSight == false && _enemyHP._dead == false)
        {
            navMeshAgent.Stop();
            _animator.SetBool("Walking", false);
        }
        else if (_enemySight.playerInSight == true && _enemyAttack._inMeleeRange == false && _enemyHP._dead == false)
        {
            navMeshAgent.Resume();
            navMeshAgent.SetDestination(_enemySight._player.transform.position);
            _animator.SetBool("Walking", true);

        }
        else if (_enemySight.playerInSight == true && _enemyAttack._inMeleeRange == true && _enemyHP._dead == false)
        {
            navMeshAgent.Stop();
            _transform.LookAt(_playerTransform);
            _animator.SetBool("Walking", false);
        }
	}
}
