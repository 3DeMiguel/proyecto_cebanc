﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour {

    public float _meleeRange =2f;
    public bool _inMeleeRange = false;

    private EnemySight _enemySight;
    private Animator _animator;
    //private UnityEngine.AI.NavMeshAgent _navMeshAgent;

    public GameObject attack1Box, attack2Box, attack3Box;

    // Use this for initialization
    void Start () {
        _enemySight = GetComponentInChildren<EnemySight>();
        _animator = GetComponent<Animator>();
        //_navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }
	
	// Update is called once per frame
	void Update () {
		if (Vector3.Distance(_enemySight._player.transform.position, transform.position) <= _meleeRange)
        {
            _inMeleeRange = true;

            if (_inMeleeRange)
            {
                EnemyMelee();
            }
        }
        else
        {
            _inMeleeRange = false;
             if (_inMeleeRange==false)
            {
                EnemyOutOfMelee();
            }
        }
	}

    private void EnemyMelee()
    {
        _animator.SetBool("Attacking", true);
    }
    private void EnemyOutOfMelee()
    {
        _animator.SetBool("Attacking", false);
    }

    public void AttackBoxOn()
    {
        attack1Box.gameObject.SetActive(true);
    }

    public void AttackBoxOff()
    {
        attack1Box.gameObject.SetActive(false);
    }
}
