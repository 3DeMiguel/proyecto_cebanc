﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Highlight : MonoBehaviour {

    public bool _selected = false;

    // creamos un evento que se lanzará cuando 
    // un enemigo esté seleccionado
    public delegate void EnemyHighlight(Transform target);
    public static event EnemyHighlight OnEnemyHighlight;

    public Transform _selectedTransform;
    private void Awake()
    {
        // quitamos todas las suscripciones al evento OnEnemyHighlight
        OnEnemyHighlight = null;
    }

    private void Start()
    {
        _selectedTransform = GetComponent<Transform>();
    }

    private void Update()
    {

    }

    private void LateUpdate()
    {
        if (_selected == true)
        {
            SkinnedMeshRenderer[] renderers = GetComponentsInChildren<SkinnedMeshRenderer>();
            foreach (SkinnedMeshRenderer renderer in renderers)
            {
                if (renderer.material.shader.name == "Piratas/Standard Outline")
                {
                    renderer.material.SetFloat("_Outline", 0.5f);
                    
                        if (OnEnemyHighlight != null)
                        {
                            OnEnemyHighlight(_selectedTransform);
                        }
                    
                }
            }
        }
        else if (_selected == false)
        {

            SkinnedMeshRenderer[] renderers = GetComponentsInChildren<SkinnedMeshRenderer>();
            foreach (SkinnedMeshRenderer renderer in renderers)
            {
                if (renderer.material.shader.name == "Piratas/Standard Outline")
                {
                    renderer.material.SetFloat("_Outline", 0f);
                    _selected = false;
                }
            }
        }
    }

    public void Unselect()
    {
        _selected = false;
        if (OnEnemyHighlight != null)
        {
            OnEnemyHighlight(null);
        }
    }
}
