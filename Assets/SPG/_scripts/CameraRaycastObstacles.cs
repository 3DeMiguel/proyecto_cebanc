﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CameraRaycastObstacles : MonoBehaviour {

    // declaramos el evento que se lanzará cuando
    // hagamos click en el plano
    public delegate void Click(Vector3 destination);
    public static event Click Onclick;

    // creamos un evento que se lanzará cuando 
    // hagamos click sobre el plano
    public delegate void ClickWithTransform(Transform target);
    public static event ClickWithTransform OnClickWithTransform;

    // creamos un evento que se lanzará cuando 
    // un enemigo esté seleccionado
    //public delegate void EnemyHighlight(Transform target);
    //public static event EnemyHighlight OnEnemyHighlight;

    public LayerMask _layerFloor;
    public LayerMask _layerEnemies;

    public GameObject _arrowsPosition;

    public Transform _playerLastTarget;
    public Transform _playerTransform;
    private Transform _cameraTransform;
    private Camera _camera;

    public PlayerMeleeAttack _playerMeleeAttack;


    private List<GameObject> _obstaclesList;

    private void Awake()
    {
        // quitamos todas las suscripciones al evento OnClick
        Onclick = null;
        // quitamos todas las suscripciones al evento OnClickWithTransform
        OnClickWithTransform = null;
    }

    private void Start()
    {
        _obstaclesList = new List<GameObject>();
        _cameraTransform = Camera.main.GetComponent<Transform>();
        _camera = GetComponent<Camera>();

        // suscribo al evento que genera el highlight
        //Highlight.OnEnemyHighlight += Highlight_OnEnemyHighlight;
    }

    /*private void Highlight_OnEnemyHighlight(Transform target)
    {
        _playerLastTarget= target;
    }*/

    void Update()
    {
        RayObstacles();
        RayOutline();

        // miramos si hemos hecho click con el ratón
        if (Input.GetMouseButtonDown(0))
        {
            // declaramos la variable que nos informa
            // sobre la colisión del rayo
            RaycastHit hit;
            // creamos un rayo que apunta al plano usando la cámara
            Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
            // lanzamos el rayo
            if (_playerMeleeAttack._playerTarget==null)
            {
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, _layerFloor))
                {
                    // el rayo ha colisionado contra el suelo
                    if (hit.collider.tag == "floor")
                    {
                        // vacío la variable de target para que deje de ir hacia ahí
                        _playerMeleeAttack._targetAttack = null;
                        // comprobamos si hay alguna clase que va a manejar el evento
                        if (Onclick != null)
                        {
                            // lanzamos el evento con el punto en el que
                            // se ha producido la colisión
                            Onclick(hit.point);
                        }
                        // comprobamos si  hay alguna clase
                        // que vaya a manejar el evento con el transform
                        // como parámetro
                        if (OnClickWithTransform != null)
                        {
                            // lanzamos el evento del transform del objeto
                            // con el que se ha colisionado
                            OnClickWithTransform(hit.transform);
                        }
                        if (!Input.GetKey(KeyCode.LeftShift) && _playerMeleeAttack._playerTarget == null)
                        {
                            GameObject createdArrows = Instantiate(_arrowsPosition, hit.point, Quaternion.identity);
                            Destroy(createdArrows, 1f);
                        }
                    }
                    else if (hit.collider.tag == "Enemy")
                    {
                        //_playerMeleeAttack._playerAgent.SetDestination(_playerLastTarget.position);
                        _playerMeleeAttack.transform.LookAt(_playerMeleeAttack._playerTarget);
                    }
                }
            }
        }
        else if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, _layerFloor))
            {
                if (hit.collider.tag == "floor")
                {
                    if (Onclick != null)
                    {
                        Onclick(hit.point);
                    }
                    if (OnClickWithTransform != null)
                    {
                        OnClickWithTransform(hit.transform);
                    }
                }
                else if (hit.collider.tag == "Enemy")
                {
                    Debug.Log("click sobre enemigo");
                }
                else if (hit.collider.tag == "Items")
                {
                    Debug.Log("click sobre Item");
                }
            }
        }
    }

    //metodo que lanza un rayo hacia el jugador y oculta lo que este con el tag obstacle
   private void RayObstacles()
    {
        //creamos un rayo desde la camara al personaje 
        //Ray obstaclesRay = Camera.main.ScreenPointToRay(_playerTransform.position);
        //recogemos en un array todo lo que entra en el rayo
        RaycastHit[] allHits = Physics.RaycastAll(new Ray(_cameraTransform.position, _cameraTransform.forward));
        allHits = allHits.OrderBy(hit => hit.distance).ToArray();
        //miramos todos los elementos de la list que colisionan
        for (int i = 0; i < allHits.Count(); i++)
        {
            RaycastHit hit = allHits[i];
            //si el objeto de la lista es un obstaculo
            if (hit.collider.tag == "Obstacle")
            {
                //desactivamos su mesh renderer
                hit.collider.GetComponent<MeshRenderer>().enabled = false;
                if (!_obstaclesList.Contains(hit.collider.gameObject))
                {
                    _obstaclesList.Add(hit.collider.gameObject);
                }
            }
            else if (hit.collider.tag == "Player")
            {
                break;
            }
        }

        bool encontrado = false;
        for (int i = 0; i < _obstaclesList.Count; i++)
        {
            encontrado = false;
            foreach (RaycastHit hit in allHits)
            {
                if (hit.collider.gameObject.GetInstanceID() == _obstaclesList[i].GetInstanceID())
                {
                    encontrado = true;
                }
            }
            if (encontrado == false)
            {
                _obstaclesList[i].GetComponent<MeshRenderer>().enabled = true;
                _obstaclesList.RemoveAt(i);
                i--;
            }
        }
    }

    private Highlight _lastHighlight;

    private void RayOutline()
    {
        Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (_lastHighlight != null)
        {
            _lastHighlight._selected = false;
        }
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, _layerEnemies))
        {
            //si el objeto con el que choca el rayo es un enemigo
            if (hit.collider.tag == "Enemy")
            {
                _lastHighlight = hit.collider.gameObject.GetComponent<Highlight>();
                _lastHighlight._selected = true;
                //hit.collider.gameObject.GetComponent<Highlight>()._selected = true;
                //if (OnEnemyHighlight != null && _highlight._selected == true)
                //{
                 //   OnEnemyHighlight(hit.collider.GetComponent<Transform>());
                //}
            }
        }
        else if (_lastHighlight != null)
        {
            _lastHighlight.Unselect();
        }

    }
}
