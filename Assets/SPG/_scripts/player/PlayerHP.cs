﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHP : MonoBehaviour {

    public float _playerMaxHP = 1000f;
    public float _playerCurrentHP;
    public bool _dead=false;

    GameController _gameController;
    Transform _sliderTransform;

    public Slider _slider;
    public Image _fillImage;

    private Animator _animator;
    private NavMeshAgent _navMeshAgent;

    public Color _fullHPColor = Color.green;
    public Color _zeroHPColor = Color.red;

    Menu _scriptMenu;
    //public Animator _animator;

    //public Camera _camera;

    private void Awake()
    {
        GetSlider();
        _gameController = FindObjectOfType<GameController>();
        Debug.Log("qué hostias");
        Debug.Log(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 1)
        {
            _playerCurrentHP = _playerMaxHP;
            Debug.Log("PlayerHP: Es la escena de la taberna y el prota tiene: " + _playerCurrentHP + " puntos de vida.");
        }

        else if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex > 1)
        {
            _playerCurrentHP = _gameController._playerCurrentHP;
            Debug.Log("PlayerHP: No es la escena de la taberna y el prota tiene: " + _playerCurrentHP + " puntos de vida.");
        }
    }

    private void OnEnable()
    {
        // REVISAR
        //_slider = GameObject.Find("HealthSliderbueno").GetComponent<Slider>();
        /* if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 2)
         {
             _gameController._playerCurrentHP = _playerHP;
         }

         else if(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex > 2)
         {
             _playerCurrentHP = _gameController._playerCurrentHP;
         }*/
        

        _dead = false;
        SetHealthUIOnHit();
    }

    // Use this for initialization
    void Start () {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
        _scriptMenu = FindObjectOfType<Menu>();
    }
	
	// Update is called once per frame
	void Update () {
        if (_playerCurrentHP <= 0)
        {
            PlayerDeath();
        }
    }



    /*private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "EnemyAttackBox")
        {

        }
    }*/

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "EnemyAttackBox")
        {
            _playerCurrentHP -= 10f;
            _animator.SetTrigger("GetHit");
            SetHealthUIOnHit();
        }
    }

    public void SetHealthUIOnHit()
    {
        _gameController._playerCurrentHP = _playerCurrentHP;
        _slider.value = _playerCurrentHP;
        Debug.Log("Nos pegan, el HP del player es: " + _gameController._playerCurrentHP + " en el gamecontroller;" + _playerCurrentHP + " en el playerHP");
    }
    public void SetHealthUIOnHeal()
    {
        _playerCurrentHP = _gameController._playerCurrentHP;
        _slider.value = _playerCurrentHP;
        Debug.Log("Cogemos corazón y nos curamos, el HP del player es: " + _gameController._playerCurrentHP + " en el gamecontroller;" + _playerCurrentHP + " en el playerHP");
    }

    void PlayerDeath()
    {
        _dead = true;
        _navMeshAgent.enabled = false;
        _animator.SetTrigger("Death");
        _scriptMenu._PanelMuerte.SetActive(true);
    }
    void GetSlider()
    {
        GameObject[] goArray = SceneManager.GetSceneByName("_Menu").GetRootGameObjects();
        if (goArray.Length > 0)
        {
            GameObject rootGo = goArray[0];
            _sliderTransform = rootGo.transform.FindChild("CanvasPointsSPG/CanvasPointsSPGbueno/HealthSliderbueno");
            Debug.Log("el padre del slider es "+_sliderTransform);
            _slider = _sliderTransform.GetComponent<Slider>();
            Debug.Log("El slider es "+_slider);
        }
    }
}
