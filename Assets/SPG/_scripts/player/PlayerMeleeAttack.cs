﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMeleeAttack : MonoBehaviour {

    public Animator _animator;
    public NavMeshAgent _playerAgent;

    private GameController _gameController;

    public bool _playerMelee = false;

    public float _meleeRange = 3f;
    public bool _inMeleeRange;

    private PlayerHP _playerHP;

    public Transform _playerTarget;
    public Transform _targetAttack;
    private Vector3 _targetPositionUpdated;

    [Header("Attack boxes")]
    public GameObject attack1Box;
    public GameObject attackTwisterBox;

    // Use this for initialization
    void Start()
    {
        // suscribo al evento que genera el highlight
        //CameraRaycastObstacles.OnEnemyHighlight += CameraRaycastObstacles_OnEnemyHighlight;
        Highlight.OnEnemyHighlight += Highlight_OnEnemyHighlight;

        _playerHP = GetComponent<PlayerHP>();
        _gameController = FindObjectOfType<GameController>();
    }

    private void Highlight_OnEnemyHighlight(Transform target)
    {
        _playerTarget = target;
    }
    
    // Update is called once per frame
    void Update ()
    {
        if (_playerTarget!=null && Vector3.Distance(transform.position, _playerTarget.position) <= _meleeRange)
        {
            _inMeleeRange = true;
        }
        else
        {
            _inMeleeRange = false;
        }
        TwisterAttack();
        MeleeAttack();
    }

    void MeleeAttack()
    {
        if(_playerHP._dead != true)
        {
            // ataque combo
            if (Input.GetMouseButton(0) && _inMeleeRange == true && _playerTarget != null)
            {
                _animator.SetTrigger("Attack");
                //_animator.SetBool("Attacking", true);
                _playerMelee = true;
                //_animator.SetBool("Move", false);
                //_playerAgent.Stop();
                transform.LookAt(_playerTarget);
            }

            else if (Input.GetMouseButton(0) && _inMeleeRange == false && _playerTarget == null)
            {
                if (!Input.GetKey(KeyCode.LeftShift))
                {
                    _playerMelee = false;
                    _playerAgent.Resume();
                }
                else
                {
                    _playerMelee = true;
                    _animator.SetTrigger("Attack");
                    _animator.SetBool("Attacking", true);
                }

            }

            else if (Input.GetMouseButton(0) && _inMeleeRange == false && _playerTarget != null)
            {

                if (!Input.GetKey(KeyCode.LeftShift))
                {
                    _targetAttack = _playerTarget;
                    _playerAgent.SetDestination(_playerTarget.position);
                    _playerMelee = false;
                    //_playerAgent.SetDestination(_targetPositionUpdated);
                    transform.LookAt(_targetPositionUpdated);
                }
                else
                {
                    _playerMelee = true;
                    _animator.SetTrigger("Attack");
                    _animator.SetBool("Attacking", true);
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                _animator.ResetTrigger("Attack");
                _animator.SetBool("Attacking", false);
            }
            else
            {
                _playerMelee = false;
            }


            if (_targetAttack != null)
            {
                _playerAgent.SetDestination(_targetAttack.position);
            }
            //else if (Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.LeftShift) && _inMeleeRange == false && _playerTarget==null)
            //{
            //    _animator.SetTrigger("Attack");
            //}
        }

    }

    void TwisterAttack()
    {
        if (Input.GetKey(KeyCode.Space) && _gameController._playerCurrentXP > 399)
        {
            _animator.SetBool("TwisterAttack", true);
            _animator.SetBool("Attacking", true);
            _gameController._playerCurrentXP -= 5;
            _gameController._textoExperiencia.text = "EXPERIENCE: " + _gameController._playerCurrentXP.ToString();
            AttackTwisterBoxOn();
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            _animator.SetBool("TwisterAttack", false);
            _animator.SetBool("Attacking", false);
            AttackTwisterBoxOff();
        }
    }

    /*private void ChaseTarget()
    {
        
        if (_inMeleeRange == true)
        {
            _animator.SetTrigger("Attack");
        }
        else
        {
            _playerAgent.SetDestination(_playerTarget.position);
        }
    }*/

    public void AttackBox1On()
    {
        attack1Box.gameObject.SetActive(true);
    }
    
    public void AttackBox1Off()
    {
        attack1Box.gameObject.SetActive(false);
    }
    public void AttackTwisterBoxOn()
    {
        attackTwisterBox.gameObject.SetActive(true);
    }

    public void AttackTwisterBoxOff()
    {
        attackTwisterBox.gameObject.SetActive(false);
    }
}
