﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerControllerDiablo : MonoBehaviour {

    public LayerMask _layer;
    // declaramos la variable del agente de navegacion
    public NavMeshAgent _agent;

    public Animator _animator;

    private PlayerHP _playerHP;

    [Header("Audio settings")]
    private AudioSource _audiosource;

    public Camera _camera;

    public AudioSource _stepsAudioSource;
    public AudioClip _stepClip;
    // Use this for initialization
    void Start()
    {
        // nos suscribimos al evento que genera la cámara
        CameraRaycastObstacles.Onclick += CameraRaycast_Onclick;
        _camera = FindObjectOfType<Camera>();
    }

    private void CameraRaycast_Onclick(Vector3 destination)
    {
        if (_playerHP._dead != true)
        {
            // le decimos al agente que se mueva hasta el destino
            if (!Input.GetKey(KeyCode.LeftShift))
            {
                Debug.Log(GetInstanceID());
                _agent.SetDestination(destination);
            }
        }
    }

    private void OnDisable()
    {
        CameraRaycastObstacles.Onclick -= CameraRaycast_Onclick;

    }

    private void OnEnable()
    {
        _playerHP = GetComponent<PlayerHP>();
        CameraRaycastObstacles.Onclick += CameraRaycast_Onclick;
    }

    // Update is called once per frame
    void Update ()
    {
        if (_playerHP._dead == false)
        {
            if (_agent.remainingDistance > 3)
            {
                _animator.SetBool("Move", true);
            }
            else
            {
                _animator.SetBool("Move", false);
            }
            LookAtMouse();
        }
    }

    public void LookAtMouse()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            _animator.SetBool("Move",false);
            _agent.Stop();
            _agent.ResetPath();

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100,_layer))
            {
                Vector3 lookPoint = hit.point;
                lookPoint.y = transform.position.y;
                transform.LookAt(lookPoint);
            }
        }
    }

    public void StepSound()
    {
       
        //audio pisadas
        _stepsAudioSource.PlayOneShot(_stepClip);
    }
}
