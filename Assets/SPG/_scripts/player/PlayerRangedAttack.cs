﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerRangedAttack : MonoBehaviour {

    public Animator _animator;
    public Transform _playerTransform;
    public Transform _playerTarget;
    public NavMeshAgent _playerAgent;

    private PlayerHP _playerHP;
    private GameController _gameController;

    public int _startingAmmo=3;

    [Header("Projectile")]
    public GameObject _prefabProjectile;
    public GameObject _projectileExit;
    private GameObject _projectile = null;
    public Transform _transformInstancedProjectile;

    public ParticleSystem _shootParticles;
    public AudioClip _shootClip;    
    public AudioSource _gunAudioSource;
    public AudioSource _swordAudioSource;
    public AudioClip _swordClip;


    private void Awake()
    {
        _gameController = FindObjectOfType<GameController>();
    }
    // Use this for initialization
    void Start ()
    {
        // suscribo al evento que genera el highlight
        Highlight.OnEnemyHighlight += Highlight_OnEnemyHighlight;

        _playerHP = GetComponent<PlayerHP>();
    }

    private void Highlight_OnEnemyHighlight(Transform target)
    {
        _playerTarget = target;
    }

    // Update is called once per frame
    void Update ()
    {
        if (_playerHP._dead == false)
        {
            if (Input.GetMouseButtonDown(1))
            {
                /*if (_playerTarget != null)
                {
                    _playerAgent.Stop();
                    transform.rotation = Quaternion.Euler(_playerTarget.rotation.eulerAngles * 1f);
                    transform.LookAt(_playerTarget);
                    _playerAgent.ResetPath();
                    _animator.SetBool("Move", false);
                    _animator.SetTrigger("Shooting");
                }
                else if (_playerTarget == null)
                {*/
                _playerAgent.Stop();
                _playerAgent.ResetPath();
                _playerAgent.updateRotation = false;
                    
                //}
            }
        }
    }

    private void LateUpdate()
    {
        if (Input.GetMouseButtonDown(1))
        {
            _animator.SetBool("Move", false);
            _animator.SetTrigger("Shooting");

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100))
            {
                Vector3 lookPoint = hit.point;
                lookPoint.y = transform.position.y;
                transform.LookAt(lookPoint);
            }
        }
    }

    void Shoot()
    {
        if (_gameController._currentAmmo > 0)
        {
            _projectile = (GameObject)Instantiate(_prefabProjectile, _transformInstancedProjectile.position, _transformInstancedProjectile.rotation);
            Rigidbody projectileRigidBody = _projectile.GetComponent<Rigidbody>();

            AddParticlesShoot();

            /*if (_playerTarget != null)
            {
                Vector3 playerToTarget = _transformInstancedProjectile.position - _playerTarget.position;
                projectileRigidBody.AddForce(playerToTarget * 1500f);
            }
            else
            {*/
            projectileRigidBody.AddForce(_playerTransform.forward * 1500f);
            StartCoroutine(EnableAgentRotation());
            _gameController._currentAmmo -= 1;
            _gameController._textoMunicion.text = "X: " + _gameController._currentAmmo;
            //}
        }
        else
        {
            Debug.Log("Sin munición.");
            StartCoroutine(EnableAgentRotation());
        }

    }

    private IEnumerator EnableAgentRotation()
    {
        yield return new WaitForSeconds(0.7f);
        _playerAgent.updateRotation = true;
    }

    private void AddParticlesShoot()
    {
        Debug.Log("emision de particulas tiro");
        _shootParticles.Play();
        //hacemos visible las particulas
        _shootParticles.gameObject.SetActive(true);
        //audio disparo
        _gunAudioSource.PlayOneShot(_shootClip);
    }
    public void AudioSword()
    {
        //audio espada
        _swordAudioSource.PlayOneShot(_swordClip);
    }
}
