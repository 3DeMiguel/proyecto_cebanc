﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineOnMouseOverMeshRenderer : MonoBehaviour {

    //public delegate void EnemyHighlight(Transform target);
    //public static event EnemyHighlight OnEnemyHighlight;

    public float _minWidth;
    public float _maxWidth;

    public void OnMouseOver()
    {
        if (transform.tag == "Items")
        {
            MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();
            foreach (MeshRenderer renderer in renderers)
            {
                if (renderer.material.shader.name == "Piratas/Standard Outline")
                {
                    renderer.material.SetFloat("_Outline", _maxWidth);
                }
            }
        }

        /*if (OnEnemyHighlight != null)
        {
            OnEnemyHighlight(GetComponent<Transform>());
        }*/
    }

    public void OnMouseExit()
    {
        MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer renderer in renderers)
        {
            if (renderer.material.shader.name == "Piratas/Standard Outline")
            {
                renderer.material.SetFloat("_Outline", _minWidth);
            }
        }
        /*if (OnEnemyHighlight != null)
        {
            OnEnemyHighlight(null);
        }*/
    }
}
