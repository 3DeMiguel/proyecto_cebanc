﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    private Animator _animator;
    private UnityEngine.AI.NavMeshAgent _navMeshAgent;
    public GameObject _player;

    void Awake()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
    }
    // Use this for initialization
    void Start () {
        _animator = GetComponentInChildren<Animator>();
        _navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }
	
	// Update is called once per frame
	void Update () {

        if (Vector3.Distance(_player.transform.position, transform.position) <= 5)
        {
            _animator.SetBool("Following", false);
            _navMeshAgent.Stop();
        }
        else
        {
            _navMeshAgent.Resume();
            _navMeshAgent.SetDestination(_player.transform.position);
            _animator.SetBool("Following", true);
        }

    }
}
