﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerControllerDiablo_j : MonoBehaviour {

    public LayerMask _layer;
    // declaramos la variable del agente de navegacion
    public NavMeshAgent _agent;

    public Animator _animator;

    [Header("Audio settings")]
    private AudioSource _audiosource;

    public Camera _camera;

    public AudioSource _stepAudioSource;
    public AudioClip _stepClip;

    // Use this for initialization
    void Start()
    {
        // nos suscribimos al evento que genera la cámara
        CameraRaycastObstacles.Onclick += CameraRaycast_Onclick;
    }

    private void CameraRaycast_Onclick(Vector3 destination)
    {
        // le decimos al agente que se mueva hasta el destino
        if (!Input.GetKey(KeyCode.LeftShift))
        {
            Debug.Log(GetInstanceID());
            _agent.SetDestination(destination);
        }
    }

    private void OnDisable()
    {
        CameraRaycastObstacles.Onclick -= CameraRaycast_Onclick;

    }

    private void OnEnable()
    {
        CameraRaycastObstacles.Onclick += CameraRaycast_Onclick;
    }

    // Update is called once per frame
    void Update () {
		if (_agent.remainingDistance > 3)
        {
            _animator.SetBool("Move", true);
        }
        else
        {
            _animator.SetBool("Move", false);
        }
        LookAtMouse();
    }

    public void LookAtMouse()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            _animator.SetBool("Move",false);
            _agent.Stop();
            _agent.ResetPath();

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100,_layer))
            {
                Vector3 lookPoint = hit.point;
                lookPoint.y = transform.position.y;
                transform.LookAt(lookPoint);
            }
        }
    }
    public void StepSound()
    {
        //audio pisadas
        _stepAudioSource.PlayOneShot(_stepClip);
    }
}
