﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMeleeAttack_j : MonoBehaviour {

    public Animator _animator;
    public NavMeshAgent _playerAgent;

    public bool _playerMelee = false;

    public float _meleeRange = 2f;
    public bool _inMeleeRange;

    public Transform _playerTarget;
    public Transform _targetAttack;
    private Vector3 _targetPositionUpdated;

    [Header("Attack boxes")]
    public GameObject attack1Box;

    // Use this for initialization
    void Start()
    {
        // suscribo al evento que genera el highlight
        //CameraRaycastObstacles.OnEnemyHighlight += CameraRaycastObstacles_OnEnemyHighlight;
        Highlight.OnEnemyHighlight += Highlight_OnEnemyHighlight;
    }

    private void Highlight_OnEnemyHighlight(Transform target)
    {
        _playerTarget = target;
    }
    
    // Update is called once per frame
    void Update ()
    {
        if (_playerTarget!=null && Vector3.Distance(transform.position, _playerTarget.position) <= _meleeRange)
        {
            _inMeleeRange = true;
        }
        else
        {
            _inMeleeRange = false;
        }

        MeleeAttack();
    }

    void MeleeAttack()
    {
        // ataque combo
        if (Input.GetMouseButton(0) && _inMeleeRange ==true && _playerTarget!=null)
        {
            _animator.SetTrigger("Attack");
            //_animator.SetBool("Attacking", true);
            _playerMelee = true;
            //_animator.SetBool("Move", false);
            //_playerAgent.Stop();
            transform.LookAt(_playerTarget);
        }
        
        else if (Input.GetMouseButton(0) && _inMeleeRange == false && _playerTarget == null)
        {
            if (!Input.GetKey(KeyCode.LeftShift))
            {
                _playerMelee = false;
                _playerAgent.Resume();
            }
            else
            {
                _playerMelee = true;
                _animator.SetTrigger("Attack");
                _animator.SetBool("Attacking", true);
            }
            
        }

        else if (Input.GetMouseButton(0) && _inMeleeRange == false && _playerTarget != null)
        {
            
            if (!Input.GetKey(KeyCode.LeftShift))
            {
                _targetAttack = _playerTarget;
                _playerAgent.SetDestination(_playerTarget.position);
                _playerMelee = false;
                //_playerAgent.SetDestination(_targetPositionUpdated);
                transform.LookAt(_targetPositionUpdated);
            }
            else
            {
                _playerMelee = true;
                _animator.SetTrigger("Attack");
                _animator.SetBool("Attacking", true);
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            _animator.ResetTrigger("Attack");
            _animator.SetBool("Attacking", false);
        }
        else
        {
            _playerMelee = false;
        }


        if (_targetAttack != null)
        {
            _playerAgent.SetDestination(_targetAttack.position);
        }
        //else if (Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.LeftShift) && _inMeleeRange == false && _playerTarget==null)
        //{
        //    _animator.SetTrigger("Attack");
        //}
    }

    /*private void ChaseTarget()
    {
        
        if (_inMeleeRange == true)
        {
            _animator.SetTrigger("Attack");
        }
        else
        {
            _playerAgent.SetDestination(_playerTarget.position);
        }
    }*/

    public void AttackBox1On()
    {
        attack1Box.gameObject.SetActive(true);
    }
    
    public void AttackBox1Off()
    {
        attack1Box.gameObject.SetActive(false);
    }
}
