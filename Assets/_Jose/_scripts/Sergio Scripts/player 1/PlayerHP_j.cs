﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class PlayerHP_j : MonoBehaviour {

    private float _playerHP = 1000f;
    public float _playerCurrentHP;

    public Slider _slider;
    public Image _fillImage;

    private NavMeshAgent _navMeshAgent;

    private Canvas _canvasSlider;

    public Color _fullHPColor = Color.green;
    public Color _zeroHPColor = Color.red;

    //public Animator _animator;

    //public Camera _camera;

    // Use this for initialization
    void Start () {
        _playerCurrentHP = _playerHP;
        _canvasSlider = GetComponent<Canvas>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () {

    }

    /*private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "EnemyAttackBox")
        {

        }
    }*/

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "EnemyAttackBox")
        {
            _playerCurrentHP -= 10f;
            SetHealthUI();
        }
    }

    public void SetHealthUI()
    {
        _slider.value = _playerCurrentHP;
    }
}
