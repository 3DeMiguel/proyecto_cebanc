﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerRangedAttack_j : MonoBehaviour {

    public Animator _animator;
    public Transform _playerTransform;
    public Transform _playerTarget;
    public NavMeshAgent _playerAgent;

    private float _ammo=3f;
    public float _currentAmmo;

    [Header("Projectile")]
    public GameObject _prefabProjectile;
    public GameObject _projectileExit;
    private GameObject _projectile = null;
    public Transform _transformInstancedProjectile;

    public ParticleSystem _shootParticles;
    public AudioClip _shootClip;
    public AudioClip _stepClip;
    public AudioSource _gunAudioSource;
    public AudioSource _stepsAudioSource;

    // Use this for initialization
    void Start ()
    {
        // suscribo al evento que genera el highlight
        Highlight.OnEnemyHighlight += Highlight_OnEnemyHighlight;
    }

    private void Highlight_OnEnemyHighlight(Transform target)
    {
        _playerTarget = target;
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetMouseButtonDown(1))
        {
            if (_playerTarget != null)
            {
                transform.LookAt(_playerTarget);
                _playerAgent.ResetPath();
                _animator.SetBool("Move", false);
                _animator.SetTrigger("Shooting");
            }
            else if (_playerTarget == null)
            {
                _playerAgent.ResetPath();
                _animator.SetBool("Move", false);
                _animator.SetTrigger("Shooting");

                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit, 100))
                {
                    Vector3 lookPoint = hit.point;
                    lookPoint.y = transform.position.y;
                    transform.LookAt(lookPoint);
                }
            }
        }
    }

    void Shoot()
    {
        _projectile = (GameObject)Instantiate(_prefabProjectile, _transformInstancedProjectile.position, _transformInstancedProjectile.rotation);
        Rigidbody projectileRigidBody = _projectile.GetComponent<Rigidbody>();

        AddParticlesShoot();

        if (_playerTarget != null)
        {
            //transform.LookAt(_playerTarget);
            projectileRigidBody.AddForce(_playerTarget.position * 1500f);
        }
        else
        {
            projectileRigidBody.AddForce(_playerTransform.forward * 1500f);
        }
    }
    private void AddParticlesShoot()
    {
        Debug.Log("emision de particulas tiro");
        _shootParticles.Play();
        //hacemos visible las particulas
        _shootParticles.gameObject.SetActive(true);
        //audio disparo
        _gunAudioSource.PlayOneShot(_shootClip);
    }


}
