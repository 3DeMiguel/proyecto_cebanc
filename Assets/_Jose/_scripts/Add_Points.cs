﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Add_Points : MonoBehaviour {

    //vamos a crear los eventos para sumar monedas recogidas
    //public event System.EventHandler OnSumaCoins;

    //para el sonido
    private AudioSource _audioSource;
     
    //punto de donde saldrá el texto
    public Transform _transformInstanciaText;

    //variables del objeto que se va a recoger
    public GameObject _visibleText;

    private Quaternion _cameraRotation;

    private PlayerHP _playerHP;
    //variable del script donde se van a sumar los puntos
    private GameController _gameController;
    private bool _interactable = false;
    private Collider _collider;

    private void Awake()
    {
        //antes de suscribirnos tenemos que resetear
        // Add_Points.OnSumaCoins = null;
        _gameController = FindObjectOfType<GameController>();
        _collider = GetComponent<Collider>();
        _cameraRotation = Camera.main.transform.rotation;
    }

    private void OnEnable()
    {
        _playerHP = FindObjectOfType<PlayerHP>();
    }
    // Use this for initialization
    void Start ()
    {
        _audioSource = GetComponent<AudioSource>();
        StartCoroutine(SetInteractable());
    }

    private IEnumerator SetInteractable()
    {
        yield return new WaitForSeconds(1.0f);
        _interactable = true;
    }
	
	

    //para hacer saltar evento de recogida de coins al entrar en contacto con el
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag== "Player" && _interactable == true)
        {
            Destroy();
            _interactable = false;
            _collider.enabled = false;
            //hacemos que aparezca cuantos puntos, monedas, etc ha recogido el jugador
            GameObject ItemType = (GameObject)Instantiate(_visibleText, _transformInstanciaText.position, _cameraRotation);
            //Destruimos este objeto tras un tiempo ya que es solo para visualizar cuantos puntos han sido
            Destroy(ItemType, 1f);

            if (ItemType.tag ==("Coins"))
            {
                _gameController.OnSumaCoins();
            }
           else if (ItemType.tag == ("Experience"))
            {
                _gameController.OnSumaExperience();
            }
            else if (ItemType.tag == ("Munition"))
            {
                _gameController.OnSumaAmmo();
            }
            else if (ItemType.tag == ("Food"))
            {
                _gameController.OnSumaFood();
            }
            else if (ItemType.tag == ("Live"))
            {
                _gameController.OnSumaLive();
                _playerHP.SetHealthUIOnHeal();
            }

            //eliminamos el objeto
            // Destroy(gameObject);

        }
    }


    private void Destroy()
    {
        Destroy(this.gameObject);
    }


    /*
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && _interactable == true)
        {
            _interactable = false;
            _collider.enabled = false;

            //hacemos que aparezca cuantos puntos, monedas, etc ha recogido el jugador
            GameObject CoinsNumber = (GameObject)Instantiate(_visibleText, _transformInstanciaText.position, _transformInstanciaText.rotation);
            //Destruimos este objeto tras un tiempo ya que es solo para visualizar cuantos puntos han sido
            //aqui le quiero meter una translacion haci arriba en el tiempo antes de destruirlo
            Destroy(CoinsNumber, 1f);

            _gameController.OnSumaCoins();
            
           
            //eliminamos el objeto
            Destroy(this.gameObject);
        }

    }
    */
}
