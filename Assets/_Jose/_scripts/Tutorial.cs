﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Tutorial : MonoBehaviour {

    public GameObject _canvasTutorial;
    private bool _toDoTutorial;

    public GameObject _paneltutorial1;
    public GameObject _paneltutorial2;
    public GameObject _paneltutorial3;
    public GameObject _paneltutorial4;

    private Menu _scriptsMenu;
    private GameController _gameController;
    // Use this for initialization
    void Start () {
        _canvasTutorial.SetActive(false);
        _toDoTutorial = true;
        _scriptsMenu = FindObjectOfType<Menu>();
        _gameController= FindObjectOfType<GameController>();
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void activateTutoria()
    {
        if (Time.timeScale == 1.0f)
        {
            Debug.Log("PAUSA");
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1f;
        }

        _canvasTutorial.SetActive(true);
    }

    public void desactivatePanel1()
    {
        _paneltutorial1.SetActive(false);
    }
    public void desactivatePanel2()
    {
        _paneltutorial2.SetActive(false);
    }
    public void desactivatePanel3()
    {
        _paneltutorial3.SetActive(false);
    }
    public void desactivatePanel4()
    {
        _paneltutorial4.SetActive(false);
    }
    public void activatePanel1()
    {
        _paneltutorial1.SetActive(true);
    }
    public void activatePanel2()
    {
        _paneltutorial2.SetActive(true);
    }
    public void activatePanel3()
    {
        _paneltutorial3.SetActive(true);
    }
    public void activatePanel4()
    {
        _paneltutorial4.SetActive(true);
    }



    public void hideCanvas()
    {
            
            Time.timeScale = 1f;
       
        _canvasTutorial.SetActive(false);
    }
    // Update is called once per frame
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Player" && _toDoTutorial == true)
        {
            activateTutoria();
            _toDoTutorial = false;

        }

    }

    public void activatePanelPointsPlayer()

    {
        _scriptsMenu._panelPointsPlayer.SetActive(true);
    }

    public void playerPointsInitial()
    {
        _gameController._textoExperiencia.text = "Experience: " +_gameController._playerCurrentXP.ToString();
        _gameController._textoMunicion.text = "Ammo: " + _gameController._currentAmmo;
    }
}

          
           
