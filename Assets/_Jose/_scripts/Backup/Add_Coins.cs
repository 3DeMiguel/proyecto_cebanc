﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Add_Coins : MonoBehaviour {

    //vamos a crear los eventos para sumar monedas recogidas
    //public event System.EventHandler OnSumaCoins;

    //para el sonido
    private AudioSource _audioSource;
     
    //punto de donde saldrá el 
    public Transform _transformInstanciaItem;

    //variables del objeto que se va a recoger
    public GameObject _visibleCoinsText;
    public GameObject _visibleExperienceText;
    public GameObject _visibleMunitionText;
    public GameObject _visibleFoodText;


    //variable del script donde se van a sumar los puntos
    public GameController _gameController;
    private bool _interactable = false;
    private Collider _collider;

    private void Awake()
    {
        //antes de suscribirnos tenemos que resetear
        // Add_Points.OnSumaCoins = null;
        _gameController = FindObjectOfType<GameController>();
        _collider = GetComponent<Collider>();
    }

    // Use this for initialization
    void Start ()
    {
        _audioSource = GetComponent<AudioSource>();
        StartCoroutine(SetInteractable());
    }

    private IEnumerator SetInteractable()
    {
        yield return new WaitForSeconds(1.5f);
        _interactable = true;
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}


    //para hacer saltar evento de recogida de coins al entrar en contacto con el
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag== "Player" && _interactable == true)
        {
            _interactable = false;
            _collider.enabled = false;
            //hacemos que aparezca cuantos puntos, monedas, etc ha recogido el jugador
            GameObject CoinsNumber = (GameObject)Instantiate(_visibleCoinsText, _transformInstanciaItem.position, _transformInstanciaItem.rotation);
            //Destruimos este objeto tras un tiempo ya que es solo para visualizar cuantos puntos han sido
            Destroy(CoinsNumber, 1f);

            _gameController.OnSumaCoins();
            
                     
            //HACER SALTAR EL EVENTO DE SUMAR estrellas lanzadas
            //if (OnSumaCoins != null)
            //{
            //    OnSumaCoins(this, System.EventArgs.Empty); 
            //}

            //SONIDO cuando lo tengamos
        //    _audioSource.PlayOneShot(_audios._shootAudio);

            //eliminamos el objeto
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && _interactable == true)
        {
            _interactable = false;
            _collider.enabled = false;

            //hacemos que aparezca cuantos puntos, monedas, etc ha recogido el jugador
            GameObject CoinsNumber = (GameObject)Instantiate(_visibleCoinsText, _transformInstanciaItem.position, _transformInstanciaItem.rotation);
            //Destruimos este objeto tras un tiempo ya que es solo para visualizar cuantos puntos han sido
            //aqui le quiero meter una translacion haci arriba en el tiempo antes de destruirlo
            Destroy(CoinsNumber, 1f);

            _gameController.OnSumaCoins();


            //HACER SALTAR EL EVENTO DE SUMAR estrellas lanzadas
            //if (OnSumaCoins != null)
            //{
            //    OnSumaCoins(this, System.EventArgs.Empty); 
            //}

            //SONIDO cuando lo tengamos
            //    _audioSource.PlayOneShot(_audios._shootAudio);

            //eliminamos el objeto
            Destroy(this.gameObject);
        }

    }
}
