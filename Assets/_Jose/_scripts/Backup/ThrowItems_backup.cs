﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowItems_backup : MonoBehaviour {

    //script general para poder meter a diferentes animaciones para que salten los eventos de lanzar los diferentes items
    public Animator _chestAnimator;
    public AudioSource _audioSource;
    public AudioClip _audioCoinClip;
    public Animation _chestAnimation;
    
    //variables para sacar el item
    private bool _ItemClicked;
    public GameObject _prefabItem;
    public GameObject _prefabExperience;
    public GameObject _prefabMunition;
    public GameObject _prefabFood;

    public Rigidbody _RigidBodyCoins;
    public Rigidbody _RigidBodyExperience;
    public Rigidbody _RigidBodyMunition;
    public Rigidbody _RigidBodyFood;

    public Transform _transformInstanciaItem;
 
    public Vector3 _direccionSalida;
    public float _velocidadSalida;

        
    // Update is called once per frame
    void Update()
    {


    }

    //para ver si el personaje esta cerca del cofre para soltar los items
    private void OnTriggerEnter(Collider collider)
    {
        //comprobamos si el tag que ha entrado es el del player
        if (collider.gameObject.tag == "Player")
        {
            Debug.Log("El player esta cerca del ITEM");
            //hacemos saltar la animación del baul abriendose
            _chestAnimator.SetTrigger("OpenChest");
       
        }
    }

    //metodos que se lanzaran con el evento de la animacion del cofre
    public void ThrowCoinsITEM()
    {
        
        {            
                //instanciamos el prefab del item    
                GameObject ItemCoins = (GameObject)Instantiate(_prefabItem, _transformInstanciaItem.position, _transformInstanciaItem.rotation);

                //recuperamos el rigidBody del item instanciado
                Rigidbody RigidBodyItemCoins = ItemCoins.GetComponent<Rigidbody>();

                //aplicamos fuerza al item
                _direccionSalida.x = Random.Range(_direccionSalida.x - 0.2f, _direccionSalida.x + 0.2f);
            RigidBodyItemCoins.AddForce(transform.TransformDirection(_direccionSalida * _velocidadSalida));

                //SONIDO
                // _audioSource.PlayOneShot(_audios._CoinsAudio);
            
        }
    }

    public void ThrowExperienceITEM()
    {

        {
            //instanciamos el prefab del item    
            GameObject ItemExperience = (GameObject)Instantiate(_prefabExperience, _transformInstanciaItem.position, _transformInstanciaItem.rotation);

            //recuperamos el rigidBody del item instanciado
            Rigidbody RigidBodyItemExperience = ItemExperience.GetComponent<Rigidbody>();

            //aplicamos fuerza al item
            RigidBodyItemExperience.AddForce(_direccionSalida * _velocidadSalida);

            //SONIDO
            // _audioSource.PlayOneShot(_audios._ExperienceAudio);

        }
    }
    public void ThrowMunitionITEM()
    {

        {
            //instanciamos el prefab del item    
            GameObject ItemMunition = (GameObject)Instantiate(_prefabMunition, _transformInstanciaItem.position, _transformInstanciaItem.rotation);

            //recuperamos el rigidBody del item instanciado
            Rigidbody RigidBodyItemMunition = ItemMunition.GetComponent<Rigidbody>();

            //aplicamos fuerza al item
            RigidBodyItemMunition.AddForce(_direccionSalida * _velocidadSalida);

            //SONIDO
            // _audioSource.PlayOneShot(_audios._shootAudio);

        }
    }
    public void ThrowFoodITEM()
    {

        {
            //instanciamos el prefab del item    
            GameObject ItemFood = (GameObject)Instantiate(_prefabFood, _transformInstanciaItem.position, _transformInstanciaItem.rotation);

            //recuperamos el rigidBody del item instanciado
            Rigidbody RigidBodyItemFood = ItemFood.GetComponent<Rigidbody>();

            //aplicamos fuerza al item
            RigidBodyItemFood.AddForce(_direccionSalida * _velocidadSalida);

            //SONIDO
            // _audioSource.PlayOneShot(_audios._FoodAudio);

        }
    }

}
