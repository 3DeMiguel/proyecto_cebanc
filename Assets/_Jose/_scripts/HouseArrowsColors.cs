﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseArrowsColors : MonoBehaviour {

    public GameObject _flechaEntry; 

	// Use this for initialization
	void Start () {
        _flechaEntry.SetActive(false);

    }

    // Update is called once per frame
    void Update ()
    {
		
	}

    private void OnTriggerStay(Collider collider)
    {
        //comprobamos si el tag que ha entrado es el del player
        if (collider.gameObject.tag == "Player")
        {

            _flechaEntry.SetActive(true);

        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            _flechaEntry.SetActive(false);

        }
    }
}
