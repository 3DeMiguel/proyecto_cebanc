﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class FlechaANiveles : MonoBehaviour {

    public GameObject _flechaRoja;
    public GameObject _flechaVerde;
    private PersistentGameObject _persistentScript;


    //variables de escenas a habilitar
    public GameObject _actualScene;
    public nivelesEnum _nivelToGo;
    public string _nameLevelactivate;


    private bool _canEntry;
   
    //creamos evento para cambair de escenas
    //public delegate void CambioEscena(nivelesEnum destino, string nombreObjeto);
    //public static event CambioEscena OnCambioEscena;


    private void Awake()
    {
        //antes de suscribirnos tenemos que resetear
        //FlechaANiveles.OnCambioEscena = null;


    }

    // Use this for initialization
    void Start ()
    {
        _persistentScript = GameObject.FindObjectOfType<PersistentGameObject>();
        _flechaRoja.SetActive(true);
        _flechaVerde.SetActive(false); 
    }
	
	// Update is called once per frame
	void Update ()
    {
       
	}

    public void OnMouseOver()
    {
        _flechaRoja.SetActive(false);
        _flechaVerde.SetActive(true);
        _canEntry = true;
    }
    public void OnMouseExit()
    {
        _flechaRoja.SetActive(true);
        _flechaVerde.SetActive(false);
        _canEntry = false;
    }
    public void OnMouseDown()
    {
        if (_canEntry == true)
        {
            //para desactivar la visibilidad de la localizion actual
            _actualScene.SetActive(false);
            _persistentScript.CambioEscena(_nivelToGo, _nameLevelactivate);
            //HACER SALTAR EL EVENTO de cambiar de escena
            //if (OnCambioEscena != null)
            //{
            //    OnCambioEscena(_nivelToGo, _nameLevelactivate);
            //}
        }
        

    }
}
