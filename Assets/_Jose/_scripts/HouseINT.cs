﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseINT : MonoBehaviour {

    public GameObject _upstairs;
    private bool _inUpstairs;
	// Use this for initialization

	void Start ()
    {
        _upstairs.SetActive(false);	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Player")
        {
            if (!_upstairs.activeSelf)
            {
                _upstairs.SetActive(true);
            }
            else
            {
                _upstairs.SetActive(false);
            }
        }
    }
}
