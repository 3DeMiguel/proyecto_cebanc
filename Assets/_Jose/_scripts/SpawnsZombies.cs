﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnsZombies : MonoBehaviour {

    //Game Object a instanciar en el spawn
    public GameObject _spawnZombie;
    private bool _interactableSpawn;

    //tiempo para nuevo spawn
    private float _spawnTime;
    public float _maxTimeRespawn =90f;
    // Use this for initialization
    void Start ()
    {
        //empezamos cone el spawn activo y siempre por debajo de cero hasta que es activado
        _spawnTime = 0;
        _interactableSpawn = true;	
	}

    private void Update()
    {
        //restamos tiempo al contador del spawn
        _spawnTime -= Time.deltaTime;

        //si el tiempo de spawn es cero o menor 
        if (_spawnTime <= 0)
        {
            _interactableSpawn = true;
        }
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Player" && _interactableSpawn == true)
        {
            _spawnTime = _maxTimeRespawn;
            _interactableSpawn = false;
            IstantiateZombieSpawn();
        }

    }

    //metodo para instanciar el prefab del spawn
    void IstantiateZombieSpawn()
    {
        GameObject ZombieSpawn = Instantiate(_spawnZombie, transform.position, transform.rotation);
        ZombieSpawn.transform.parent = transform;
    }
}
