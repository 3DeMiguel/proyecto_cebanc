﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class Menu : MonoBehaviour {

    // Declaramos la imagen que se rellenará con
    // el porcentaje del nivel cargado
    public Image _loadingImage;

    // Declaramos una variable que nos indica el proceso
    // de carga de la escena en segundo plano
    private AsyncOperation _gameLoadingOperation;
    //vinculamos al script que tiene el canvas para hacerlo visible.
    PersistentGameObject _persistentScript;

    public GameObject _Menu;
    public GameObject _PanelCreditos;
    public GameObject _PanelOpciones;
    public GameObject _PanelHistory;
    public GameObject _PanelMuerte;
    public GameObject _panelPausa;
    public GameObject _panelPointsPlayer;
    //public GameObject _PanelPuntosJuego;

    GameController _scriptGameController;
    PlayerHP _scriptPlayerHP;

  //  public Text _textMonedas;
  //  public Text _textExperience;
  // public Text _textFood;
 //   public Text _textMunition;

  //  private int _monedas;
  //  private int _experience;
  //  private int _munition;
  //  private int _food;



    private void Start()
    {
        _scriptGameController = FindObjectOfType<GameController>();
        _scriptPlayerHP = FindObjectOfType<PlayerHP>();
      //  _Menu = FindObjectsOfType<Menu>();
    }
    public void LoadGame()
	{
       
        // de manera asíncrona
        StartCoroutine(LoadGameAsync());
	}

    // Creamos una corutina para poder cargar el nivel
    // en segundo plano
    private IEnumerator LoadGameAsync()
    {

      //  yield return new WaitForSeconds(5);
        // Iniciamos el proceso de carga de manera asíncrona de las escenas
        //_gameLoadingOperation = SceneManager.LoadSceneAsync(2); //escena taberna
        _gameLoadingOperation = SceneManager.LoadSceneAsync(2, LoadSceneMode.Additive); //escena taberna
       // _gameLoadingOperation.allowSceneActivation = false;
       // yield return new WaitForSeconds(1);
        _gameLoadingOperation = SceneManager.LoadSceneAsync(3, LoadSceneMode.Additive); //escena Town    
        _gameLoadingOperation = SceneManager.LoadSceneAsync(4, LoadSceneMode.Additive); //escena house1       
        _gameLoadingOperation = SceneManager.LoadSceneAsync(5, LoadSceneMode.Additive); //escena house2
        _gameLoadingOperation = SceneManager.LoadSceneAsync(6, LoadSceneMode.Additive); //escena house3
        _gameLoadingOperation = SceneManager.LoadSceneAsync(7, LoadSceneMode.Additive); //escena casa 4 laura
        _gameLoadingOperation = SceneManager.LoadSceneAsync(8, LoadSceneMode.Additive); //escena barco



        //_gameLoadingOperation.allowSceneActivation = false;
        // yield return new WaitForSeconds(1);

        //_gameLoadingOperation = SceneManager.LoadSceneAsync(6, LoadSceneMode.Additive); //escena CanvasPuntos

        HideMenu();
        //retornamos el resultado de la corrutina
        yield return _gameLoadingOperation;

        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(2));

        //activamos el canvas del script persistent
       // _persistentScript._canvas.SetActive(true);

        //ocultamos el menu de inicio.
       // HideMenu();
    }

   /* //para que tarde mas, olbigandole a parar un tiempo en la barra de cargado
    private IEnumerator WaitTime()
    {
     new WaitForSeconds(1);
        yield return WaitTime();
    }*/

    private void Update()
    {
        // Nos aseguramos de que la variable de carga del nivel
        // tiene valor
        if (_gameLoadingOperation != null)
        {
            // Rellenamos la imagen con el porcentaje cargado
            // del nivel
            _loadingImage.fillAmount = _gameLoadingOperation.progress;
            
            _PanelCreditos.SetActive(true);
        }

        //para cuando mueres
        if (_scriptPlayerHP._dead==true)
        {
            Debug.Log("Muerto0");
            _PanelMuerte.SetActive(true);

        }


        //igualamos las variables de las estadisticas del juego actual
       // _monedas = _scriptGameController._coins;
      //  _food = _scriptGameController._food;
      //  _experience = _scriptGameController._experience;
      //  _munition = _scriptGameController._currentAmmo;


        //sumamos los datos a los textos de los paneles de muertes y pausa
      //  _textMonedas.text =  _monedas.ToString();
       // _textMunition.text = _munition.ToString();
      //  _textFood.text = _food.ToString();
      //  _textExperience.text = _experience.ToString();

    }
    public void HideMenu()
    {
        _Menu.SetActive(false);
    }
    public void showMenu()
    {
        _Menu.SetActive(true);

    }

    public void Exit()
	{
		Application.Quit();
	}

    //metodos para ocultar visibilizar los diferentes paneles del canvas
    public void HidePanelOpciones()
    {
        _PanelOpciones.SetActive(false);
    }
    public void HidePanelCreditos()
    {
        _PanelCreditos.SetActive(false);
    }
    public void ShowPanelOpciones()
    {
        _PanelOpciones.SetActive(true);
    }
    public void ShowPanelCreditos()
    {
        _PanelCreditos.SetActive(true);
    }
    public void ShowPanelHistory()
    {
        _PanelHistory.SetActive(true);
    }
    public void HidePanelHistory()
    {
        _PanelHistory.SetActive(false);
    }
    /*public void ShowPanelPuntos()
    {
        _PanelPuntosJuego.SetActive(true);
    }
    public void HidePanelPuntos()
    {
        _PanelPuntosJuego.SetActive(false);
    }*/
    public void activateCanvasPointPlayer()
    {
        _panelPointsPlayer.SetActive(true);
    }
    public void desactivateCanvasPointPlayer()
    {
        _panelPointsPlayer.SetActive(false);
    }
    public void ShowPanelPausa()
    {
        _panelPausa.SetActive(true);
    }
    public void HidePanelPausa()
    {
        _panelPausa.SetActive(false);
    }
    public void showPanelMuerte()
    {
        _PanelMuerte.SetActive(true);
    }
    public void HidePanelMuerte()
    {
        _PanelMuerte.SetActive(false);
    }
    public void StopTime()
    {
        if (Time.timeScale == 1.0f)
        {
            Debug.Log("PAUSA");
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1f;
        }

    }
}
