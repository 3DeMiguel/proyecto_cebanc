﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PersistentGameObject : MonoBehaviour {

    public nivelesEnum _nivelesEnum;
    private SortedList<nivelesEnum, Scene> _escenas;
        

    private int _sceneToCharge;

    private bool _inSceneTabern;
    private bool _inSceneTown;
    private bool _inSceneHouse;
    private bool _inSceneBoat;
    
   
    // Use this for initialization
    void Start () {

        //_canvas.SetActive(false);

        // Marcamos el GameObject para que no se destruya
        // entre el cambio de escenas
        DontDestroyOnLoad(this.gameObject);
        // Cargamos la escena del menu
        SceneManager.LoadScene(1);

        //recogemos todas las escenas que hay en una lista
        _escenas = new SortedList<nivelesEnum, Scene>();
        _escenas.Add(nivelesEnum.house1, SceneManager.GetSceneByBuildIndex((int)nivelesEnum.house1));
        _escenas.Add(nivelesEnum.house2, SceneManager.GetSceneByBuildIndex((int)nivelesEnum.house2));
        _escenas.Add(nivelesEnum.house3, SceneManager.GetSceneByBuildIndex((int)nivelesEnum.house3));
        _escenas.Add(nivelesEnum.house4, SceneManager.GetSceneByBuildIndex((int)nivelesEnum.house4));
        _escenas.Add(nivelesEnum.tabern, SceneManager.GetSceneByBuildIndex((int)nivelesEnum.tabern));
        _escenas.Add(nivelesEnum.town, SceneManager.GetSceneByBuildIndex((int)nivelesEnum.town));
        _escenas.Add(nivelesEnum.boat, SceneManager.GetSceneByBuildIndex((int)nivelesEnum.boat));

    }

    public void CambioEscena(nivelesEnum destino, string nombreObjeto)
    {
        //activamos la escena destino
        Scene escenaActivar = SceneManager.GetSceneByBuildIndex((int)destino);
        SceneManager.SetActiveScene(escenaActivar);
        escenaActivar.GetRootGameObjects()[0].SetActive(true);
    }
     
   
    private void WherePlayerIs()
    {
        if (_inSceneTabern == true)
        {
            _inSceneTown = false;
            _inSceneHouse = false;
            _inSceneBoat = false;
        }
        if (_inSceneTown == true)
        {
            _inSceneTabern = false;
            _inSceneHouse = false;
            _inSceneBoat = false;
        }
        if (_inSceneHouse == true)
        {
            _inSceneTown = false;
            _inSceneTabern = false;
            _inSceneBoat = false;
        }
        if (_inSceneBoat == true)
        {
            _inSceneTown = false;
            _inSceneHouse = false;
            _inSceneTabern = false;
        }
    }
}
