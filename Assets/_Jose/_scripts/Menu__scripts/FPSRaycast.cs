﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSRaycast : MonoBehaviour {

    // Declaramos un origen para el rayo
    [SerializeField] // SerializeField hace que variables 
                     //privadas salgan por el inspector
    private Transform _raycastOrigin;

    // Creamos un contador para saber cuántos objetos hay que buscar
    private int _paperCount;

    private void Start()
    {
        // Recuperamos todos los GameObjects que tengan el tag
        // de la hoja
        GameObject[] allPapers = GameObject.FindGameObjectsWithTag("Paper");
        // Contamos cuántas hojas hay en total
        _paperCount = allPapers.Length;
    }

    private void Update()
    {
        // Decimos que se dibuje un rayo en la vista de escena
        Debug.DrawRay(_raycastOrigin.position, _raycastOrigin.forward);
        // Comprobamos si hemos pulsado la tecla e para lanzar el rayo
        if (Input.GetKeyDown(KeyCode.E))
        {
            // Creamos la variable para almacenar la información
            // del rayo
            RaycastHit hitInfo;
            // Lanzamos el rayo
            if (Physics.Raycast(_raycastOrigin.position, _raycastOrigin.forward, out hitInfo))
            {
                // Comprobamos si lo que ha colisionado contra el rayo
                // es la hoja
                if (hitInfo.collider.tag == "Paper")
                {
                    // Destruimos el objeto
                    Destroy(hitInfo.collider.gameObject);
                    // Restamos la hoja encontrada
                    _paperCount -= 1;
                    // Comprobamos si hemos encontrado todas las hojas
                    if (_paperCount == 0)
                    {
                        // Mostramos un mensaje diciendo que hemos
                        // salvado el mundo
                        Debug.Log("Hemos terminado. Somos los mejores.");
                    }
                }
            }
        }
    }
}
