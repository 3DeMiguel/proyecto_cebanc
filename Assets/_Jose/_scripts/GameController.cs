﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    [Header("textos que salen en pantalla al coger item")]
    //variables textos para el canvas
    public Text _textoCoins;
    public Text _textoVidas;
    public Text _textoExperiencia;
    public Text _textoMunicion;
    public Text _textoFood;
    public Text _textoLive; //esto lo haremos con una barra e imagenes del pirata


    [Header("textos menu Pausa")]
    //variables textos para el canvas
    public Text _textoCoinsPausa;
   // public Text _textoVidasPausa;
    public Text _textoExperienciaPausa;
   // public Text _textoMunicionPausa;
    public Text _textoFoodPausa;

    [Header("textos menu Muerte")]
    //variables textos para el canvas
    public Text _textoCoinsMuerte;
   // public Text _textoVidasMuerte;
    public Text _textoExperienciaMuerte;
   // public Text _textoMunicionMuerte;
    public Text _textoFoodMuerte;

    [Header("Cantidad de puntos")]
    //variables cantidas de objetos que se  recogen por item
    public int _coins = 1;
    public int _experience = 100;
    public int _food = 1;
    public int _ammo = 1;
    public float _life = 50f;

    [Header("Player atributes")]
    //variables privadas de los objetos y puntuacion recogidas
    private float _playerCoins = 0f;
    public float _playerCurrentHP;
    public int _playerCurrentXP=0;
    public int _currentAmmo;
    private float _playerFood;

    public PlayerHP _playerHP;
    private PlayerRangedAttack _playerRangedAttack;

    [Header("Cips de audios")]
    public AudioSource _audioSource;
    public AudioClip _clipComida;
    public AudioClip _clipCoins;
    public AudioClip _clipAddLife;
    public AudioClip _clipExperience;
    public AudioClip _clipMunition;
    public AudioClip _clipDisparo;
    public AudioClip _clipPisadas;
    public AudioClip _clipZombieAudio;


    private void Awake()
    {
        _playerRangedAttack = FindObjectOfType<PlayerRangedAttack>();
        //if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex > 0)
        //{
        _playerHP = FindObjectOfType<PlayerHP>();
        _playerCurrentHP = _playerHP._playerCurrentHP;
        Debug.Log("GameController: Es la escena de la taberna y el prota tiene: " + _playerCurrentHP + " puntos de vida.");

        //}

    }

    private void OnEnable()
    {


    }

    // Use this for initialization
    void Start ()
    {
        //suscribiremos los eventos de donde se lanzan
        //suscribimos el evento de sumar COINS
        // Add_Points.OnSumaCoins += OnSumaCoins;
                _currentAmmo = _playerRangedAttack._startingAmmo;
        _textoExperiencia.text = "Experience: " + _playerCurrentXP;
        _textoMunicion.text = "Ammo: " + _currentAmmo + 3;
        _textoCoins.text = "GOLD : " + _playerCoins;
        _textoFood.text = "Provisiones: " + _playerFood.ToString();
    }

    private void Update()
    {
        
    }

    //evento de sumar monedas
    public void OnSumaCoins()
    {
        //sumamos coins a nuestra variable de monedas
        _playerCoins += _coins;
        //sacamos la variable por texto
        _textoCoins.text = "GOLD : " + _playerCoins.ToString();
        _textoCoinsPausa.text = "GOLD : " + _playerCoins.ToString();
        _textoCoinsMuerte.text = "GOLD : " + _playerCoins.ToString();

        //audio
        _audioSource.PlayOneShot(_clipCoins);
    }
    public void OnSumaExperience()
    {
        //sumamos xp a nuestra variable de experiencia
        _playerCurrentXP += _experience;
        //sacamos la variable por texto
        _textoExperiencia.text = "EXPERIENCE: " + _playerCurrentXP.ToString();
        _textoExperienciaPausa.text = "EXPERIENCE: " + _playerCurrentXP.ToString();
        _textoExperienciaMuerte.text = "EXPERIENCE: " + _playerCurrentXP.ToString();
        //_playerXP._currentXP = _playerCurrentXP;
        //audio
        _audioSource.PlayOneShot(_clipExperience);
    }
    public void OnSumaAmmo()
    {
        //sumamos coins a nuestra variable de monedas
        _currentAmmo += _ammo;
        //sacamos la variable por texto
        _textoMunicion.text = "X :" + _currentAmmo.ToString();
        //audio
        _audioSource.PlayOneShot(_clipMunition);
    }
    public void OnSumaFood()
    {
        //sumamos coins a nuestra variable de monedas
        _playerFood += _food;
        //sacamos la variable por texto
        _textoFood.text = "Provisiones: " + _playerFood.ToString();
        _textoFoodPausa.text = _playerFood.ToString();
        _textoFoodMuerte.text = _playerFood.ToString();
        //audio
        _audioSource.PlayOneShot(_clipComida);
    }

    public void OnSumaLive()
    {
        //sumamos vida al jugador
        if (_playerCurrentHP <= 950f)
        {
            _playerCurrentHP += _life;
        }
        else
        {
            _playerCurrentHP = 1000f;
        }
        

        //audio
        _audioSource.PlayOneShot(_clipAddLife);

        //sacamos la variable por texto
        //_textoLive.text = "live: " + _life.ToString();
    }
}
