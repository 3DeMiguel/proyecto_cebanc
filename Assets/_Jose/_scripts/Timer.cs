﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Timer : MonoBehaviour {

    public Text _timer;
    public float _tiempo = 30f;

	// Use this for initialization
	void Start ()
    {
        _timer.text = " BEER TIME:" + _tiempo;
    }
	
	// Update is called once per frame
	void Update ()
    {
        _tiempo -= Time.deltaTime;
        TimeSpan timesSpan = TimeSpan.FromSeconds(_tiempo);
        _timer.text = string.Format( " {0}'{1}''", timesSpan.Minutes, timesSpan.Seconds.ToString("00"));
       // _contador.text = "" + _tiempo.ToString("f0");
	}
}
