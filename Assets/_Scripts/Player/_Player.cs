﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _Player : MonoBehaviour {
    
    //para cambiar la direccion de la bala disparada
    private _disparoPlayer _sciptDisparoPlayer;

    //variable rigidbody del jugador
    public Rigidbody _playerRigidBody;
    public Transform _playerLookingDirection;
    private Transform _playerTransform;
    public Transform _transformVistaDerecha;
    public Transform _transformVistaIzquierda;
  

    [Header("Player settings")]
    public float _velocidadMovimiento;
    public float _fuerzaSalto;
    public float _velocidadShift = 2;
    public bool _tocandoSuelo = true;


    /*[Header ("ShootBullet Settings")] 
    public GameObject _prefabGunBullet;        //añadimos una variable que nos indica el prefab a  utilizar como Bullet
    public Rigidbody _RigidBodyBullet;
    public float _velocidadBullet;
    public Vector3 _direccionBullet;
    public Transform _transformInstanciaBullet;     //añadimos una variable que nos indica el lugar donde se crean las instancias de la estrella
    */

    [Header("cadencia disparos")]
    private float _tiempoTranscurrido;
    public float _tiempoDispararPistola;
    public float _tiempoDispararEspada;
    public float _tiempoMinimoDisparo;
    public float _tiempoMaximoDisparo;
    public float _tiempoDisparar;
   
    [Header ("Sounds Settings")]
    // variable que contendra el componente AudioSource
    private AudioSource _audioSource;
    public AudioClip _saltoClip;
    public AudioClip _pasosClip;
    public AudioClip _golpeEspadaClip;
    public AudioClip _disparoClip;


    [Header("Animator Clips")]
    public Animator _animator;


    //variable para saber si esta mirando derecha o izq
    public bool _playerLookingRight=true;


    // Use this for initialization
    void Start ()
    {
        //para tener control del tiempo ejecutado para los disparos
        _tiempoTranscurrido += Time.deltaTime;

        //recuperamos los componentes que necesitamos
        _playerRigidBody = GetComponent<Rigidbody>();
        _playerTransform = GetComponent<Transform>();
        _audioSource = GetComponent<AudioSource>();
        _playerLookingDirection = GetComponent<Transform>();
        _audioSource = GetComponent<AudioSource>();

    }
	
	// Update is called once per frame
	void Update ()
    {
        //para tener control del tiempo ejecutado para los disparos
        _tiempoTranscurrido += Time.deltaTime;
              

        Salto();
        Ataques();
    }

  
   
    void OnCollisionEnter(Collision col)
    {
        //para saber si estamos tocando el suelto para saltar
        if (col.gameObject.tag == "floor")
        {
            _tocandoSuelo = true;
        }
      
    }
      
    //MOVIMIENTO
    private void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 desplazamiento = new Vector3(horizontal,0f, vertical);

        if (horizontal > 0)
        {
            //instanciamos sonido de pasos    
          //  _audioSource.PlayOneShot(_pasosClip);

            _playerTransform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else if (horizontal < 0)
        {
            
            _playerTransform.rotation = Quaternion.Euler(0, 180, 0);
        }

        if (horizontal== 0 && vertical== 0)
        {
            //instanciamos sonido de pasos    
         //   _audioSource.PlayOneShot(_pasosClip);

            _animator.SetTrigger("idle");
            _animator.ResetTrigger("walk");
        }
        else
        {
            _playerTransform.Translate(desplazamiento * Time.deltaTime * _velocidadMovimiento, Space.World);
            _animator.SetTrigger("walk");
            _animator.ResetTrigger("idle");
        }

           
    } //HECHO

    //Para girar el personaje al movernos hacia la izquierda
    void PlayerLookingDirection()
    {
 
        if (Input.GetAxis("Horizontal") < 0)
        {
            //llamamos al animator de transicion enre una animacion y otra
            _animator.SetTrigger("walk");
            _animator.ResetTrigger("idle");

            _playerLookingRight = false;
            _playerLookingDirection.LookAt( _transformVistaIzquierda);


        }
        if (Input.GetAxis("Horizontal")>0)
        
        {
            _playerLookingRight = true;
            _playerLookingDirection.LookAt(_transformVistaDerecha);
            
        }
    } //HECHO
    
    
    //Para SALTAR
    void Salto()
    {

        //Para SALTAR

        if (Input.GetKeyDown(KeyCode.Space) && _tocandoSuelo == true)
        {
            Debug.Log("se ha pretado el spacio");

            //instanciamos sonido de la espada         
            _audioSource.PlayOneShot(_saltoClip);

            _animator.SetTrigger("jump");
            _animator.ResetTrigger("walk");

            _tocandoSuelo = false;


            //AUDIO SALTO
            //_audioSource.PlayOneShot(_audios._jumpAudio);

            Vector3 desplazamiento = new Vector3(0f, _fuerzaSalto, 0f);
            _playerRigidBody.velocity = desplazamiento;

        }
    } //HECHO

    //Para ATACAR
    void Ataques() //POR TERMINAR
    {
        //vamos a poner el golpear con el control
        if (Input.GetKeyDown(KeyCode.Z)&&_tiempoTranscurrido>=_tiempoDispararEspada)
        {
            //instanciamos sonido de la espada         
            _audioSource.PlayOneShot(_golpeEspadaClip);

             //activamos el box del HitPlayer

            //activamos la animacion de golpear y desactivamos el resto de animaciones
            _animator.SetTrigger("hitSword");
            _animator.ResetTrigger("idle");
            _animator.ResetTrigger("walk");                 


        }

        if (Input.GetKeyDown(KeyCode.X)&& _tiempoTranscurrido >= _tiempoDispararPistola)
        {
          
            //activamos la animacion de golpear y desactivamos el resto de animaciones
            _animator.SetTrigger("hitGun");
            _animator.ResetTrigger("idle");
            // _animator.ResetTrigger("walk");

            //reseteamos el tiempo para disparar de nuevo
            _tiempoTranscurrido = 0f;
        }

        
    }

    void RestarSalud() //PARA HACER
    {
        //aqui pondremos como se le resta la salud
    }

    void SumarPuntos() //PARA HACER
    {
        //aqui sumaremos los puntos cuando los coja
    }

  
}
