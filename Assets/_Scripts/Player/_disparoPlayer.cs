﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _disparoPlayer : MonoBehaviour



{
    public _Player _ScriptPlayer;
    
    [Header("ShootBullet Settings")]
    public GameObject _prefabGunBullet;        //añadimos una variable que nos indica el prefab a  utilizar como Bullet
    public Rigidbody _RigidBodyBullet;
    public float _velocidadBullet;
    public Vector3 _vectorBullet;
    public Transform _transformInstanciaBullet;
    public AudioSource _audioSource;
    public AudioClip _disparoClip;
    public int _directionBullet=1;

void Disparo()
    {
        
        //instanciamos sonido de disparo         
        _audioSource.PlayOneShot(_disparoClip);

        //instanciamos el proyectil de la bala
        GameObject GunBullet = (GameObject)Instantiate(_prefabGunBullet, _transformInstanciaBullet.position, _transformInstanciaBullet.rotation);
        Rigidbody bulletRigidbody = GunBullet.GetComponent<Rigidbody>();
        bulletRigidbody.AddForce(_transformInstanciaBullet.forward * _velocidadBullet);

              
    }

   
}
