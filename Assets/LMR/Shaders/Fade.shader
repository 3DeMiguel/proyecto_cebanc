﻿Shader "Piratas/Fade" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
	_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_Bump("Normal", 2D) = "bump" {}
	_BumpPower("Normal power", Range(0, 2)) = 1.0
		_Dissolve("Dissolve", 2D) = "white" {}
	_DissolveScale("Dissolve percentage", Range(0,1)) = 0.0
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200
		Cull Off

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
#pragma target 3.0

		sampler2D _MainTex;
	sampler2D _Dissolve;
	sampler2D _Normal;
	sampler2D _Bump;

	struct Input {
		float2 uv_MainTex;
	};

	half _Glossiness;
	half _Metallic;
	half _BumpPower;
	fixed4 _Color;
	half gradient;
	half _DissolveScale;

	void surf(Input IN, inout SurfaceOutputStandard o) {
		// Albedo comes from a texture tinted by color
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		gradient = tex2D(_Dissolve, IN.uv_MainTex).r;
		clip(gradient - _DissolveScale);
		o.Albedo = c.rgb;
		fixed3 normal = UnpackNormal(tex2D(_Bump, IN.uv_MainTex));
		normal.z /= _BumpPower;
		o.Normal = normalize(normal);
		// Metallic and smoothness come from slider variables
		o.Metallic = _Metallic;
		o.Smoothness = _Glossiness;
		o.Alpha = c.a;
	}
	ENDCG
	}
		FallBack "Diffuse"
}
